//
//  Pokemon.swift
//  pokemon App
//
//  Created by amr on 2/21/18.
//  Copyright © 2018 amr. All rights reserved.
//

import Foundation

class Pokemon {
    var _name : String!
    var _height : Int!
    var _weight : Int!
    var _isHidden : String!
    var _slot : Int!
    
    
    var name : String{
        if _name == nil{
            return ""
        }
        return _name
    }
    var height : Int {
        if _height == nil{
            return -1
        }
        return _height
    }
    
    var weight : Int{
        if _weight == nil{
            return -1
        }
        return _weight
    }
    var isHidden : String {
        if _isHidden == nil{
            return ""
        }
        return _isHidden
    }
    var slot : Int {
        if _slot == nil{
            return -1
        }
        return _slot
    }
    
    init(resDict : Dictionary<String, Any>) {
        
        if let nameData = resDict["name"]{
            _name = nameData as! String
        }
        if let heightData = resDict["height"]{
            _height = heightData as! Int
        }
        if let weightData = resDict["weight"]{
            _weight = weightData as! Int
        }
        if let appilitiesArray = resDict["abilities"] as? Dictionary<String, Any>
        {
            if let isHiddenData = appilitiesArray["is_hidden"]
            {
                _isHidden = isHiddenData as! String
            }
            if let slotData = appilitiesArray["slot"]
            {
                _slot = slotData as! Int
            }
        }
    }
    
}
