//
//  pokiesListVC.swift
//  pokemon App
//
//  Created by amr on 2/20/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit
import Alamofire

class pokiesListVC: UIViewController {
    var pokeID = -1
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var slotLbl: UILabel!
    @IBOutlet weak var isHiddenLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var pokeName: UILabel!
    @IBOutlet weak var pokeImg: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = "http://pokeapi.co/api/v2/pokemon/\(pokeID)"
        
        loadingIndicator.startAnimating()
        pokeImg.image = UIImage(named: "\(pokeID)")
        connectToApi(url: url)
    }

    
    //MARK: - Networking
    func connectToApi(url : String){
        Alamofire.request(URL(string : url)!).responseJSON { (response) in
            if let resultDict = response.result.value as? Dictionary<String, Any>{
                self.UpdateUI(resDict: resultDict)
                print (resultDict)
               
            }
        }
    }
    

    //MARK: - Updating the UI with Data
    func UpdateUI(resDict : Dictionary<String, Any>){
        loadingIndicator.stopAnimating()
        let poke = Pokemon(resDict: resDict)
        pokeName.text = poke.name
        heightLbl.text = "\(poke.height)"
        weightLbl.text = "\(poke.weight)"
        isHiddenLbl.text = "\(poke.isHidden)"
        slotLbl.text = "\(poke.slot)"
    }

  

}
