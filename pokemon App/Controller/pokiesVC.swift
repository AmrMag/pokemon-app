//
//  ViewController.swift
//  pokemon App
//
//  Created by amr on 2/20/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

class pokiesVC: UIViewController , UICollectionViewDelegate, UICollectionViewDataSource {
   
    // MARK: - Outlets
    //*****************************************************************************
    @IBOutlet weak var pokiesCV: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pokiesCV.dataSource = self
        pokiesCV.delegate = self
       
    }


    
    // MARK: - Collection View Functions
    //*****************************************************************************
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 50
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pokeCell", for: indexPath) as! pokeCell
        cell.pokeImage.image = UIImage(named: "\(indexPath.row + 1)")
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "List2Details", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "List2Details"
        {
        let pokiesListVC = segue.destination as! pokiesListVC
            pokiesListVC.pokeID = pokiesCV.indexPathsForSelectedItems![0].row + 1
        }
    }
    


}

